The `grotthecheat/yaml-config-package-installer-plugin` package is composer
plugin that can be used to build a centralised application configuration file,
from config files that are included in subcomponent packages.

By including a `config.yaml` file like the example below in the root of a
package, when **required** the plugin looks for this file and, if found, copies
the defined settings into a centralised config file.

    vendor: grotthecheat
    package: mysql
    settings:
        - server
        - port
        - schema
        - username
        - password

This example will be transferred into the application config as:

    grotthecheat:
        mysql:
            server: null
            port: null
            schema: null
            username: null
            password: null

By default, the application config is located in the level above the vendor
directory,
which typically is the root folder of the application, however if this is not
the case then a specific location can be added by adding an `extra` section to
your projects composer.json file.

    "extra" : {
        "yaml-config-application-config": "../folder/application_config.yaml"
    }

> **NOTE:** This custom path for the application config is relative to the
> composer vendor directory.