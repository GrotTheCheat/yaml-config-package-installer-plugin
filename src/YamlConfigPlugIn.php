<?php
namespace GrotTheCheat\YamlConfigPackageInstallerPlugin;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer\PackageEvent;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use GrotTheCheat\YamlConfig\YamlConfig;

class YamlConfigPlugIn implements PluginInterface, EventSubscriberInterface
{
    /** @var Composer */
    protected $composer;

    /** @var IOInterface */
    protected $io;

    /** @noinspection PhpUnused */
    public function activate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    /** @noinspection PhpUnused */
    public function deactivate(Composer $composer, IOInterface $io)
    {
        // TODO: Implement deactivate() method.
    }

    /**
     * Get the application config path.
     *
     * The default application config path is config.yaml in the root of the application (this is normally the folder
     * above the /vendor/ folder.  This can be changed using an extra:
     *
     * "extra" : {
     *     "yaml-config-application-config" : "app_config.yaml"
     * }
     *
     * @return string The application config path
     */
    private function getApplicationConfigPath() : string
    {
        $config_yaml_path = $this->composer->getPackage()->getExtra()['yaml-config-application-config'] ?? 'application-settings.yaml';

        return sprintf('%s/../%s',
            $this->composer->getConfig()->get('vendor-dir'),
            $config_yaml_path
        );
    }

    /** @noinspection PhpUnused */
    public static function getSubscribedEvents() : array
    {
        return [
            "post-package-install" => ["onPostPackageInstall", 0]
        ];
    }

    /**
     * Checks, prompts and initialises an application config file.
     *
     * @return bool true if the config file exists, false otherwise
     */
    private function initApplicationConfig() : bool
    {
        $application_config_path = $this->getApplicationConfigPath();

        if (!file_exists($application_config_path)) {
            $initialise = $this->io->ask('Would you like to initalise an application config file [<comment>yes</comment>]? ', 'yes');

            if ($initialise === 'yes') {
                file_put_contents($application_config_path, '');
                $this->io->write('  - Created application config: <info>' . $application_config_path . '</info>');
            } else {
                return  false;
            }
        } else {
            $this->io->write("<info>Application config file found</info>");
        }

        return true;
    }

    private function installFromPackage(string $yaml_path, string $package_name) : bool
    {
        $package_config = YamlConfig::load($yaml_path);
        $required_keys = [
            'vendor',
            'package',
            'settings'
        ];

        if (!$package_config->keysExists($required_keys)) {
            $this->io->write("<error>The package config.yaml file must contain the root keys: vendor, package and settings</error>");
            return false;
        }

        if (!is_array($package_config->get('settings'))) {
            $this->io->write("<error>The package config.yaml settings, MUST contain a list of values</error>");
            return false;
        }

        $application_config_path = $this->getApplicationConfigPath();
        $application_config = YamlConfig::load($application_config_path);

        $package_prefix = sprintf('%s.%s.',
            $package_config->get('vendor'),
            $package_config->get('package')
        );

        foreach ($package_config->get('settings') as $setting) {
            $key = $package_prefix . $setting;
            $application_config->set($key, null);
        }

        $application_config->dump();

        $this->io->write('  - Installed <info>' . $package_name . '</info> package settings');

        return true;
    }

    /** @noinspection PhpUnused */
    public function onPostPackageInstall(PackageEvent $event)
    {
        $this->io->write('<info>Running yaml-config-package-installer for: ' . $event->getOperation()->getPackage()->getName() . '</info>');

        $new_package_path = sprintf('%s/%s',
            $this->composer->getConfig()->get("vendor-dir"),
            $event->getOperation()->getPackage()->getName());

        $package_config_path = $new_package_path . '/config.yaml';

        if (file_exists($package_config_path)) {
            $this->io->write('  - Package <info>' . $event->getOperation()->getPackage()->getName() . '</info> config.yaml: found');

            if ($this->initApplicationConfig()) {
                $this->installFromPackage($package_config_path, $event->getOperation()->getPackage()->getName());
            }
        } else {
            $this->io->write('  - Package <info>' . $event->getOperation()->getPackage()->getName() . '</info> config.yaml: not found');
        }
    }

    /** @noinspection PhpUnused */
    public function uninstall(Composer $composer, IOInterface $io)
    {
        // TODO: Implement uninstall() method.
    }

}